import { combineReducers, configureStore } from '@reduxjs/toolkit';

import { devicesReducer as devices } from './devices';
import { positionsReducer as positions } from './positions';
import { usersReducer as users } from './users';

const reducer = combineReducers({
  devices,
  positions,
  users,
});

export { devicesActions } from './devices';
export { positionsActions } from './positions';
export { usersActions } from './users';

export default configureStore({ reducer });
